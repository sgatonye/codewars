package main

import (
  "strings"
  "log"
  )


func SpinWords(str string) string {
  //Check if word has mote than 5 letters
  s := strings.Split(str, " ")
  var word string
  for _, x := range s {
	if len(x) >= 5 { 
		x = reverse(x)	
	}
	word = word + x + " "
 }
	return strings.Trim(word, " ")
}

func reverse(x string) string {
	chars := []rune(x)
		for i, j := 0, len(chars)-1; i < j; i, j = i+1, j-1 {
			chars[i], chars[j] = chars[j], chars[i]
		}
		return string(chars)
}


func main(){
	log.Println(SpinWords("or making or pizza. could I like"))
}

// Need to look at other ways to reverse 